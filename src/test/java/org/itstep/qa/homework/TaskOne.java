package org.itstep.qa.homework;
/*Реализовать программный код, который по адресу
        http://hflabs.github.io/suggestions-demo/
       // заполняет все обязательные поля и отправляет сообщение
        В конце выполнить проверку, на наличие кнопки и ВСЕГО
        текста в красном блоке*/

//сделали в классе


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class TaskOne {
    public static void main(String[]arg) throws InterruptedException {
           System.setProperty ("webdriver.chrome.driver",
                    "src\\main\\resources\\chromedriver.exe");
        WebDriver driver= new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("http://hflabs.github.io/suggestions-demo/");
        WebElement element=
                driver.findElement(By.xpath("//*[@id=\"fullname\"]"));
        element.click();
        element.sendKeys("Иванов Иван Иванович" + Keys.ENTER);
         element= driver.findElement(By.id("fullname-surname"));
        element.sendKeys("Иванов");
        element= driver.findElement(By.id("fullname-name"));
        element.sendKeys("Иван");
        element= driver.findElement(By.id("email"));
        element.sendKeys("ivan@mail.ru");
        element= driver.findElement(By.id("message"));
        element.sendKeys("Thank you very much");
        driver.findElement(By.cssSelector(".btn-primary")).click();
        Thread.sleep(5000);
        element= driver.findElement(By.xpath("//*[@id=\"feedback-message\"]/h4"));
        Assert.assertEquals(element.getText(),"Это не настоящее правительство :-(");
        element= driver.findElement(By.xpath("//*[@id=\"feedback-message\"]/p[1]"));
        Assert.assertEquals(element.getText(),"К сожалению, мы не можем принять ваше обращение.\n"+
                "Но вы всегда можете отправить его через электронную приемную правительства Москвы.");
    }
}

